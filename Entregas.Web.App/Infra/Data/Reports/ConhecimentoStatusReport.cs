﻿using Entregas.Web.App.Domain.Core;
using Entregas.Web.App.Domain.Reports;
using System.Collections.Generic;
using System.Linq;
using Entregas.Web.App.Domain.DTO;
using NHibernate;
using NHibernate.Criterion;

namespace Entregas.Web.App.Infra.Data.Reports
{
    public class ConhecimentoStatusReport : Disposable, IConhecimentoStatusReport
    {
        private readonly ISession session;

        public ConhecimentoStatusReport(ISession session)
        {
            this.session = session;
        }

        public IEnumerable<ConhecimentoStatusDTO> List(ConhecimentoStatusReportParams _params)
        {
            var filter = Restrictions.On<ConhecimentoStatusDTO>(c => c.CodigoCliente)
                            .IsIn(_params.Clientes.Select(c => c.Codigo).ToList());

            return session
                    .QueryOver<ConhecimentoStatusDTO>()
                    .Where(c => c.Data >= _params.DataInicio)
                        .And(c => c.Data <= _params.DataFim)
                        .And(filter)
                    .List();
        }
    }
}