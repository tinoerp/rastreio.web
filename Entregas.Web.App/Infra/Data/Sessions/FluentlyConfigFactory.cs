﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate.Driver;

namespace Entregas.Web.App.Infra.Data.Sessions
{
    public class FluentlyConfigFactory
    {
        public static FluentConfiguration Configure(string connectionString)
        {
            var config = OracleDataClientConfiguration
                .Oracle10.ConnectionString(connectionString)
                .Driver<OracleClientDriver>().ShowSql();

            return Fluently.Configure().Database(config);
        }
    }
}