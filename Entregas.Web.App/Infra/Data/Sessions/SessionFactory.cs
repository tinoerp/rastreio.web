﻿using Entregas.Web.App.Infra.Data.Mappings;
using NHibernate;
using System.Configuration;

namespace Entregas.Web.App.Infra.Data.Sessions
{
    public class SessionFactory
    {
        private const string key = "connectString";
        public static ISessionFactory factory;

        public static ISessionFactory Get(string conn)
        {
            if (factory == null)
            {
                conn = conn ?? ConfigurationManager.ConnectionStrings[key].ConnectionString;

                factory = FluentlyConfigFactory
                    .Configure(conn)
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<ClienteMap>())
                    .BuildSessionFactory();
            }

            return factory;
        }

        public static ISessionFactory Get() => Get(null);
    }
}