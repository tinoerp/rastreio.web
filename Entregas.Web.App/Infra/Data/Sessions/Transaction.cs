﻿using Entregas.Web.App.Domain.Core;
using NHibernate;

namespace Entregas.Web.App.Infra.Data.Sessions
{
    class Transaction : Disposable, Domain.Core.ITransaction
    {
        private NHibernate.ITransaction trans;

        public Transaction(ISession session)
        {
            trans = session.BeginTransaction();
        }

        public void Commit()
        {
            trans.Commit();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && trans != null)
            {
                trans.Dispose();
                trans = null;
            }

            base.Dispose(disposing);
        }
    }
}
