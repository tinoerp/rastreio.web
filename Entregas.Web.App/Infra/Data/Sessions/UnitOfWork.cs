﻿using Entregas.Web.App.Domain.Core;
using NHibernate;

namespace Entregas.Web.App.Infra.Data.Sessions
{
    public class UnitOfWork : Disposable, IUnitOfWork
    {
        private readonly ISession session;
        private Domain.Core.ITransaction trans;

        public UnitOfWork(ISession session)
        {
            this.session = session;
        }

        public Domain.Core.ITransaction BeginTransaction()
            => trans = new Transaction(session);
    }
}