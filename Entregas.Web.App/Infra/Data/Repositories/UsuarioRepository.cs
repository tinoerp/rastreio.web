﻿using Entregas.Web.App.Domain.Core;
using Entregas.Web.App.Domain.Entities.Usuarios;
using NHibernate;
using System.Collections.Generic;
using System.Linq;

namespace Entregas.Web.App.Infra.Data.Repositories
{
    public class UsuarioRepository : Disposable, IUsuarioRepository
    {
        private readonly ISession session;

        public UsuarioRepository(ISession session)
        {
            this.session = session;
        }

        public void Add(Usuario obj)
        {
            session.Save(obj);
        }

        public void Delete(Usuario obj)
        {
            session.Delete(obj);
        }

        public Usuario FindByEmail(string email)
            => session.QueryOver<Usuario>().Where(u => u.Email == email)
                        .List().FirstOrDefault();

        public Usuario Get(int id) => session.Get<Usuario>(id);
        public IEnumerable<Usuario> GetAll() => session.QueryOver<Usuario>().List();

        public void Update(Usuario obj)
        {
            try
            {
                session.Update(obj);
            }
            catch (NonUniqueObjectException)
            {
                session.Merge(obj);
            }
        }
    }
}