﻿using Entregas.Web.App.Domain.Core;
using Entregas.Web.App.Domain.Entities.Clientes;
using NHibernate;
using System.Linq;

namespace Entregas.Web.App.Infra.Data.Repositories
{
    public class ClienteRepository : Disposable, IClienteRepository
    {
        private readonly ISession session;

        public ClienteRepository(ISession session)
        {
            this.session = session;
        }

        public Cliente ObterPeloCnpj(string cnpj)
             => session.QueryOver<Cliente>().Where(c => c.Cnpj == cnpj)
                    .List().FirstOrDefault();
    }
}