﻿using Entregas.Web.App.Domain.DTO;
using FluentNHibernate.Mapping;

namespace Entregas.Web.App.Infra.Data.Mappings
{
    class ConhecimentoStatusDTOMap : ClassMap<ConhecimentoStatusDTO>
    {
        public ConhecimentoStatusDTOMap()
        {
            Table("CT_STATUS");

            Id(c => c.Id, "CTID");
            Map(c => c.Numero);
            Map(c => c.Data);
            Map(c => c.CodigoCliente, "CODCLI");
            Map(c => c.NomeCliente, "NOMECLI");
            Map(c => c.Destinatario, "NOMEDEST");
            Map(c => c.Cidade, "CID_DEST");
            Map(c => c.Estado, "EST_DEST");
            Map(c => c.Notas, "NUMNF");
            Map(c => c.Status, "STATUS");
            Map(c => c.StatusDescricao, "DESCSTATUS");
            Map(c => c.NomeRecebedor, "NOMERECEB");
            Map(c => c.DataEntrega, "DTENTREGA");
        }
    }
}
