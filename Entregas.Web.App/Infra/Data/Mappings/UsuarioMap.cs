﻿using Entregas.Web.App.Domain.Entities.Usuarios;
using FluentNHibernate.Mapping;

namespace Entregas.Web.App.Infra.Data.Mappings
{
    class UsuarioMap : ClassMap<Usuario>
    {
        public UsuarioMap()
        {
            Table("USUARIOS");

            Id(u => u.Id, "REGID");
            Map(u => u.Nome);
            Map(u => u.Email);
            Map(u => u.Senha);

            HasManyToMany(u => u.Clientes)
                .Table("USUARIOSCLI")
                .ParentKeyColumn("REGID")
                .ChildKeyColumn("CODIGO");
        }
    }
}