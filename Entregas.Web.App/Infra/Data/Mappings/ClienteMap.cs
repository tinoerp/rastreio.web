﻿using Entregas.Web.App.Domain.Entities.Clientes;
using FluentNHibernate.Mapping;

namespace Entregas.Web.App.Infra.Data.Mappings
{
    class ClienteMap : ClassMap<Cliente>
    {
        public ClienteMap()
        {
            Table("CLIENTES");

            Id(c => c.Codigo);
            Map(c => c.Nome);
            Map(c => c.Cnpj);
        }
    }
}