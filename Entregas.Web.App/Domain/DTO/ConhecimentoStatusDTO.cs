﻿using System;

namespace Entregas.Web.App.Domain.DTO
{
    public class ConhecimentoStatusDTO
    {
        public virtual int Id { get; set; }
        public virtual string Numero { get; set; }
        public virtual DateTime Data { get; set; }
        public virtual string CodigoCliente { get; set; }
        public virtual string NomeCliente { get; set; }
        public virtual string Destinatario { get; set; }
        public virtual string Cidade { get; set; }
        public virtual string Estado { get; set; }
        public virtual string Notas { get; set; }
        public virtual string Status { get; set; }
        public virtual string StatusDescricao { get; set; }
        public virtual string NomeRecebedor { get; set; }
        public virtual DateTime? DataEntrega { get; set; }
    }
}