﻿using Entregas.Web.App.Domain.Entities.Clientes;
using System;
using System.Collections.Generic;

namespace Entregas.Web.App.Domain.DTO
{
    public class ConhecimentoStatusReportParams
    {
        public IEnumerable<Cliente> Clientes { get; }
        public DateTime DataInicio { get; }
        public DateTime DataFim { get; }

        public ConhecimentoStatusReportParams(
            IEnumerable<Cliente> clientes,
            DateTime dataInicio,
            DateTime datafim)
        {
            Clientes = clientes;
            DataInicio = dataInicio;
            DataFim = datafim;
        }
    }
}
