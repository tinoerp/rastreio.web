﻿using Entregas.Web.App.Domain.DTO;
using System.Collections.Generic;

namespace Entregas.Web.App.Domain.Reports
{
    public interface IConhecimentoStatusReport
    {
        IEnumerable<ConhecimentoStatusDTO> List(ConhecimentoStatusReportParams _params);
    }
}
