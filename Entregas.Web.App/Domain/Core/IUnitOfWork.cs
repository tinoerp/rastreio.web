﻿using System;

namespace Entregas.Web.App.Domain.Core
{
    public interface IUnitOfWork : IDisposable
    {
        ITransaction BeginTransaction();
    }
}
