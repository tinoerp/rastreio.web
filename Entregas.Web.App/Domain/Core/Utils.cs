﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Entregas.Web.App.Domain.Core
{
    public static class Utils
    {
        public static string Criptografar(this string value)
        {
            if (string.IsNullOrEmpty(value)) return null;
            var buffer = Encoding.Default.GetBytes(value);
            string hash;

            using (var sha1 = new SHA1CryptoServiceProvider())
            {
                hash = BitConverter.ToString(sha1.ComputeHash(buffer));
            }

            return hash.Replace("-", string.Empty).ToLower();
        }
    }
}