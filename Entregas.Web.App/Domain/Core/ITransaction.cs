﻿using System;

namespace Entregas.Web.App.Domain.Core
{
    public interface ITransaction : IDisposable
    {
        void Commit();
    }
}
