﻿namespace Entregas.Web.App.Domain.Entities.Clientes
{
    public class Cliente
    {
        public virtual string Codigo { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Cnpj { get; set; }
    }
}