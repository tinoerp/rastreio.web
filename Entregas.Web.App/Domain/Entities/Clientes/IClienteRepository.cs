﻿using System;

namespace Entregas.Web.App.Domain.Entities.Clientes
{
    public interface IClienteRepository : IDisposable
    {
        Cliente ObterPeloCnpj(string cnpj);
    }
}