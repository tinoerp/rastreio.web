﻿using System.Collections.Generic;

namespace Entregas.Web.App.Domain.Entities.Usuarios
{
    public class UsuarioDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public bool Administrador { get; set; }
        public IEnumerable<string> Clientes { get; set; }
    }
}