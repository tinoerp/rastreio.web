﻿using System.Collections.Generic;

namespace Entregas.Web.App.Domain.Entities.Usuarios
{
    public interface IUsuarioRepository
    {
        void Add(Usuario obj);
        void Update(Usuario obj);
        void Delete(Usuario obj);
        IEnumerable<Usuario> GetAll();
        Usuario Get(int id);
        Usuario FindByEmail(string email);
    }
}