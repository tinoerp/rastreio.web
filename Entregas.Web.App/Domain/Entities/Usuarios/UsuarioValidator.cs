﻿using FluentValidation;
using System.Linq;

namespace Entregas.Web.App.Domain.Entities.Usuarios
{
    public class UsuarioValidator : AbstractValidator<Usuario>
    {
        public UsuarioValidator()
        {
            RuleFor(u => u.Nome).NotEmpty()
                .WithMessage("Informe o nome do usuário.");

            RuleFor(u => u.Nome).Length(1, 60)
                .WithMessage("Tamanho máximo do nome: {MaxLength} caracteres.");

            RuleFor(u => u.Email).NotEmpty()
                .WithMessage("Informe o e-mail do usuário.");

            RuleFor(u => u.Email).Length(1, 255)
                .WithMessage("Tamanho máximo do e-mail: {MaxLength} caracteres.");

            RuleFor(u => u.Email).EmailAddress()
                .WithMessage("E-mail inválido.");

            RuleFor(u => u.Senha).NotEmpty()
                .WithMessage("Informe a senha do usuário.");

            RuleFor(u => u.Clientes).Must(c => c?.Count() > 0)
                .WithMessage("Nenhum cliente informado para o usuário.");
        }
    }
}