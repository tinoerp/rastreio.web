﻿using Entregas.Web.App.Domain.Core;
using Entregas.Web.App.Domain.Entities.Clientes;
using FluentValidation.Results;
using System.Collections.Generic;

namespace Entregas.Web.App.Domain.Entities.Usuarios
{
    public class Usuario
    {
        public virtual int Id { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Email { get; set; }
        public virtual string Senha { get; set; }
        public virtual bool Administrador { get; set; }
        public virtual ICollection<Cliente> Clientes { get; set; } = new List<Cliente>();
        public virtual ValidationResult ResultadoValidacao { get; protected set; }

        public virtual bool SenhaValida(string senha)
        {
            if (string.IsNullOrEmpty(senha)) return false;
            return Senha == senha.Criptografar();
        }

        public virtual bool IsValid()
        {
            ResultadoValidacao = new UsuarioValidator().Validate(this);
            return ResultadoValidacao.IsValid;
        }
    }
}