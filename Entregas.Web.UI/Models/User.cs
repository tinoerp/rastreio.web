﻿using System.Collections.Generic;
using System.Security.Principal;

namespace Entregas.Web.UI.Models
{
    public class User : IPrincipal
    {
        private const string ADMIN_ROLE = "ADMIN";

        public IIdentity Identity { get; }
        public int Id { get; }
        public bool Administrador { get; }
        public IEnumerable<string> Clientes { get; }

        public User(
            int id, string nome, bool administrador,
            IEnumerable<string> clientes)
        {
            Identity = new GenericIdentity(nome);
            Id = id;
            Administrador = administrador;
            Clientes = clientes;
        }

        public bool IsInRole(string role)
            => role == ADMIN_ROLE && Administrador;
    }
}