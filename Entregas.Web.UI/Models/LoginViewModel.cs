﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entregas.Web.UI.Models
{
    public class LoginViewModel
    {
        [DisplayName("E-mail")]
        [Required(ErrorMessage = "Informe o e-mail.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail inválido.")]
        public string Email { get; set; }

        [DisplayName("Senha")]
        [Required(ErrorMessage = "Informe a senha.")]
        public string Senha { get; set; }
    }
}