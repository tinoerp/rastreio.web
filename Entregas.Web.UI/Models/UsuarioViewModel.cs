﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entregas.Web.UI.Models
{
    public class UsuarioViewModel
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Nome")]
        public string Nome { get; set; }

        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Senha")]
        public string Senha { get; set; }

        [DisplayName("Administrador")]
        public bool Administrador { get; set; }

        public ICollection<ClienteViewModel> Clientes { get; set; }
    }
}