﻿using System.ComponentModel;

namespace Entregas.Web.UI.Models
{
    public class ClienteViewModel
    {
        public string Codigo { get; set; }

        [DisplayName("Nome")]
        public string Nome { get; set; }

        [DisplayName("CNPJ")]
        public string Cnpj { get; set; }
    }
}