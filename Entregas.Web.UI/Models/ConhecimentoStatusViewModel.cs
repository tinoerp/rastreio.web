﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entregas.Web.UI.Models
{
    public class ConhecimentoStatusViewModel
    {
        [DisplayName("Número")]
        public string Numero { get; set; }

        [DisplayName("Data")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Data { get; set; }

        [DisplayName("Cliente")]
        public string NomeCliente { get; set; }

        [DisplayName("Destinatário")]
        public string Destinatario { get; set; }

        [DisplayName("Cidade")]
        public string Cidade { get; set; }

        [DisplayName("UF")]
        public string Estado { get; set; }

        [DisplayName("Notas Fiscais")]
        public string Notas { get; set; }

        [DisplayName("Status")]
        public string Status { get; set; }

        public string StatusDescricao { get; set; }

        [DisplayName("Recebido por")]
        public string NomeRecebedor { get; set; }

        [DisplayName("Data/hora Entrega")]
        public DateTime? DataEntrega { get; set; }
    }
}