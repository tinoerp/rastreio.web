﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entregas.Web.UI.Models
{
    public class DatasViewModel : IValidatableObject
    {
        [DisplayName("Data inicial")]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Informe a data inicial.")]
        public DateTime DataInicial { get; set; }

        [DisplayName("Data Final")]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Informe a data final.")]
        public DateTime DataFinal { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (DataInicial.CompareTo(DataFinal) == 1)
            {
                yield return new ValidationResult(
                    "A Data Final deve ser maior ou igual à Data Inicial");
            }
        }
    }
}