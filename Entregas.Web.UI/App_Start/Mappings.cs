﻿using Entregas.Web.App.Domain.DTO;
using Entregas.Web.UI.Models;
using ExpressMapper;

namespace Entregas.Web.UI
{
    public class Mappings
    {
        public static void Register()
        {
            Mapper.Register<ConhecimentoStatusDTO, ConhecimentoStatusViewModel>();
        }
    }
}