[assembly: WebActivator.PostApplicationStartMethod(typeof(Entregas.Web.UI.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace Entregas.Web.UI.App_Start
{
    using System.Reflection;
    using System.Web.Mvc;

    using SimpleInjector;
    using SimpleInjector.Integration.Web;
    using SimpleInjector.Integration.Web.Mvc;
    using App.Infra.Data.Sessions;
    using App.Domain.Core;
    using App.Domain.Entities.Clientes;
    using App.Infra.Data.Repositories;
    using App.Domain.Entities.Usuarios;
    using App.Domain.Reports;
    using App.Infra.Data.Reports;

    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            
            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            
            //container.Verify();
            
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
     
        private static void InitializeContainer(Container container)
        {
            container.Register(() => SessionFactory.Get().OpenSession(), Lifestyle.Scoped);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.Register<IClienteRepository, ClienteRepository>(Lifestyle.Scoped);
            container.Register<IUsuarioRepository, UsuarioRepository>(Lifestyle.Scoped);
            container.Register<IConhecimentoStatusReport, ConhecimentoStatusReport>(Lifestyle.Scoped);
        }
    }
}