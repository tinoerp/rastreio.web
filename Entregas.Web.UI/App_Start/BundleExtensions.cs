﻿using System.Collections.Generic;
using System.Web.Optimization;

namespace Entregas.Web.UI
{
    static class BundleExtensions
    {
        public static Bundle NonOrderFiles(this Bundle bundle)
        {
            bundle.Orderer = new AsIsBundleOrderer();
            return bundle;
        }

        class AsIsBundleOrderer : IBundleOrderer
        {
            public IEnumerable<BundleFile> OrderFiles(
                BundleContext context, IEnumerable<BundleFile> files) => files;
        }
    }
}