﻿using Entregas.Web.App.Domain.Entities.Usuarios;
using Entregas.Web.UI.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace Entregas.Web.UI.Controllers
{
    public class AccountController : BaseController
    {
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel obj, string returnUrl)
        {
            if (!ModelState.IsValid) return View(obj);
            var usuario = GetService<IUsuarioRepository>().FindByEmail(obj.Email);

            if (usuario?.SenhaValida(obj.Senha) ?? false)
            {
                Autorizar(usuario);
                return RedirectToLocal(returnUrl);
            }

            ModelState.AddModelError("", "Falha na tentativa de login.");
            return View(obj);
        }

        private void Autorizar(Usuario usuario)
        {
            var dto = new UsuarioDTO
            {
                Id = usuario.Id,
                Nome = usuario.Nome,
                Administrador = usuario.Administrador,
                Clientes = usuario.Clientes.Select(c => c.Codigo)
            };

            var userData = new JavaScriptSerializer().Serialize(dto);
            var data = DateTime.Now;

            var ticket = new FormsAuthenticationTicket(1,
                usuario.Nome,
                data,
                data.AddMinutes(FormsAuthentication.Timeout.TotalMinutes),
                false,
                userData);

            var _ticket = FormsAuthentication.Encrypt(ticket);
            Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, _ticket));
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl)) return Redirect(returnUrl);
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}