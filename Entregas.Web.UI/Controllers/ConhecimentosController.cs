﻿using Entregas.Web.App.Domain.DTO;
using Entregas.Web.App.Domain.Entities.Clientes;
using Entregas.Web.App.Domain.Reports;
using Entregas.Web.UI.Models;
using ExpressMapper;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Entregas.Web.UI.Controllers
{
    public class ConhecimentosController : BaseController
    {
        private static List<Cliente> clientes = new List<Cliente>
        {
            new Cliente { Codigo = "1881" },
            new Cliente { Codigo = "1884" },
            new Cliente { Codigo = "1885" },
            new Cliente { Codigo = "1886" }
        };

        public ActionResult Listar() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Listar(DatasViewModel obj)
        {
            if (!ModelState.IsValid)
            {
                return View(obj);
            }

            var _params = new ConhecimentoStatusReportParams(clientes, obj.DataInicial, obj.DataFinal);

            var report =
                GetService<IConhecimentoStatusReport>().List(_params)
                    .OrderBy(c => c.Notas);

            var mapped = Mapper.Map<
                IEnumerable<ConhecimentoStatusDTO>,
                IEnumerable<ConhecimentoStatusViewModel>>(report);

            return View("ListarStatus", mapped);
        }
    }
}