﻿using System.Web.Mvc;

namespace Entregas.Web.UI.Controllers
{
    public abstract class BaseController : Controller
    {
        protected static T GetService<T>()
        {
            return DependencyResolver.Current.GetService<T>();
        }
    }
}