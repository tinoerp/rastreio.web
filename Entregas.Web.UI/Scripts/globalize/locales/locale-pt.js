﻿function loadGlobalize(options) {
    if (options == undefined) {
        options = { dates: false };
    }

    var requests = [
        $.get('/Scripts/cldr/supplemental/likelySubtags.json'),
        $.get('/Scripts/cldr/main/pt/numbers.json'),
        $.get('/Scripts/cldr/supplemental/numberingSystems.json')
    ];

    if (options.dates) {
        requests.push(
            $.get('/Scripts/cldr/main/pt/ca-gregorian.json'),
            $.get('/Scripts/cldr/main/pt/timeZoneNames.json'),
            $.get('/Scripts/cldr/supplemental/timeData.json'),
            $.get('/Scripts/cldr/supplemental/weekData.json'));
    }

    $.when.apply($, requests).then(function () {
        return [].slice.apply(arguments, [0]).map(function (result) {
            return result[0];
        });
    })
    .then(Globalize.load)
    .then(function () {
        Globalize.locale('pt');
        options.execute && options.execute(Globalize);
    });
}