﻿using Entregas.Web.App.Domain.Entities.Usuarios;
using Entregas.Web.UI.Models;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace Entregas.Web.UI
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Mappings.Register();
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            var cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie == null) return;

            var ticket = FormsAuthentication.Decrypt(cookie.Value);
            var dto = new JavaScriptSerializer().Deserialize<UsuarioDTO>(ticket.UserData);

            HttpContext.Current.User = new User(
                dto.Id, dto.Nome, dto.Administrador, dto.Clientes);
        }
    }
}
