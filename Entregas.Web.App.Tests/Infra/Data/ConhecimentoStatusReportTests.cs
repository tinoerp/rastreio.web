﻿using Entregas.Web.App.Domain.DTO;
using Entregas.Web.App.Domain.Entities.Clientes;
using Entregas.Web.App.Infra.Data.Reports;
using Entregas.Web.App.Infra.Data.Sessions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Entregas.Web.App.Tests.Infra.Data
{
    [TestClass]
    public class ConhecimentoStatusReportTests
    {
        private const string conn = "Data Source=192.168.103.13/orcl;User Id=GMI2GCMW;Password=gmi2gcmw;";

        [TestMethod]
        public void ConhecimentoStatusReport_ListarClientes_Sucesso()
        {
            // Arrange
            var session = SessionFactory.Get(conn).OpenSession();
            
            var clientes = new List<Cliente>
            {
                new Cliente { Codigo = "1881" },
                new Cliente { Codigo = "1884" },
            };

            var _params = new ConhecimentoStatusReportParams(
                clientes, new DateTime(2017, 2, 1), new DateTime(2017, 2, 28));

            var report = new ConhecimentoStatusReport(session);

            // Act
            var lista = report.List(_params);
            report.Dispose();
            session.Dispose();

            // Assert
            Assert.IsTrue(lista.Any());
        }
    }
}
